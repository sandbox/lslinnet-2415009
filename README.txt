DESCRIPTION
-----------
This module provides a fully configurable horizontal accoridon that can be
placed anywhere via a block wrapper

INSTALLATION
------------
1) Download the library liteaccordion the you can find on git hub :
https://github.com/nikki/liteAccordion

2) Create a folder called liteaccordion in /sites/all/librairies and put the
content of the git repository

3) If you do not have the libraries module, download it.

4) Enable Lite Accordion module

DEVELOPERS
----------
For developers an uncompressed Javascript file has been included
For more informations https://github.com/nikki/liteAccordion
